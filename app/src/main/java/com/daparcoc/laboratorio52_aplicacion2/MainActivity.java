package com.daparcoc.laboratorio52_aplicacion2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private Button btnVolver;
    private Button btnSalvar;
    private Button imgButton;
    private EditText editText;
    private CheckBox checkBox;
    private RadioButton radio_red;
    private RadioButton radio_blue;
    private ToggleButton toggleButton;
    private RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setUpViews() {
        btnVolver = (Button) findViewById(R.id.btnVolver);
        btnSalvar = (Button) findViewById(R.id.btnSalvar);
        btnSalvar.setEnabled(false);
        imgButton = (Button) findViewById(R.id.imgButton);
        editText = (EditText) findViewById(R.id.editText);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        radio_red = (RadioButton) findViewById(R.id.radio_red);
        radio_blue = (RadioButton) findViewById(R.id.radio_blue);
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void sendClick(View v) {
        // Perform action on clicks
        String allText = new String("campo: " + editText.getText());
        allText = allText + ":checkbox:";
        if (checkBox.isChecked()) {
            allText = allText + "Checked:";
        } else {
            allText = allText + "Not Checked:";
        }

        allText = allText + ":toggle:";
        if (toggleButton.isChecked()) {
            allText = allText + "Checked:";
        } else {
            allText = allText + "Not Checked:";
        }

        allText = allText + "radios:rojo";
        String redText = "";
        if (radio_red.isChecked()) {
            redText = "pulsado:";
        } else {
            redText = "no pulsado:";
        }

        allText = allText + redText;
        allText = allText + "azul";
        String bluetext = "";
        if (radio_blue.isChecked()) {
            bluetext = "pulsado:";
        } else {
            bluetext = "no pulsado:";
        }
        allText = allText + bluetext;
        allText = allText + "rating:";
        float f = ratingBar.getRating();
        allText = allText + Float.toString(f) + ":";
        Log.d("app", allText);
        Toast.makeText(this, allText, Toast.LENGTH_LONG).show();
    }

    public void checkBoxClick(View v) {
        String text = "";
        if (checkBox.isChecked()) {
            text = "Selected";
            btnSalvar.setEnabled(true);
            Toast.makeText(this, "Ya puedes Salvar",
                    Toast.LENGTH_LONG).show();
        } else {
            btnSalvar.setEnabled(false);
            Toast.makeText(this, "Hasta que no marques la casilla " +
                    "no podrás salvar", Toast.LENGTH_LONG).show();
            text = "Not selected";
        }
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public void radioButtonClick(View v) {
        String text = "";
        if (radio_red.isChecked()) {
            text += "Red ";
        }
        if (radio_blue.isChecked()) {
            text += "Blue ";
        }
        if(!text.equals("")) {
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        }
    }

    public void toggleButtonClick(View v) {
        String text = "";
        if (toggleButton.isChecked()) {
            text = text + "Vibrate on";
        } else if (!toggleButton.isChecked()) {
            text = text + "Vibrate off";
        }
        if(!text.equals("")) {
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        }
    }

}